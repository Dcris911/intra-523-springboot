package ca.claurendeau.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Message;
import ca.claurendeau.repository.MessageRepository;

@Service
public class MessageService {
	
	@Autowired
	private MessageRepository messageRepository;
	
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}
	
	public Message findByNom(String nom) {
		List<Message> message = messageRepository.findByNom(nom);
		return message.get(0);
	}
	public Message findByEmail(String email) {
		List<Message> message = messageRepository.findByEmail(email);
		return message.get(0);
	}

}
