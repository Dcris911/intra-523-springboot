package ca.claurendeau.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.domaine.Message;
import ca.claurendeau.service.MessageService;

@RestController
public class ReactControlleur {
    
	@Autowired
	private MessageService messageService;
	
    @CrossOrigin
    @GetMapping("/nom/{nomMessage}") //VOTRE_ENDPOINT_DE_MESSAGE_ICI
    public Message retourneMessage(@PathVariable String nomMessage) {
    	return messageService.findByNom(nomMessage);
    }
    
    /* Comprends pas trop son utilite so truc basic */
    @CrossOrigin    
    @GetMapping("/index/")
    public String retourneBasicMessage() {
    	return "";
    }

     
}
