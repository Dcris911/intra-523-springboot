package ca.claurendeau.domaine;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Message {

	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private long id;
	
	private String nom;
	
	private String email;
	
	//Pour SpringBoot
	public Message() {
	}

	public Message(String nom, String email) {
		this.nom = nom;
		this.email = email;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//Pour CommandLineRunner
	@Override
	public String toString() {
		return "Message [id=" + id + ", nom=" + nom + ", email=" + email + "]";
	}
    
}
