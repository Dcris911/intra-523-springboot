package ca.claurendeau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {
	
	public List<Message> findByNom(String nom);
	public List<Message> findByEmail(String email);

}
