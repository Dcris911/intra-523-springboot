package ca.claurendeau.demo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Message;
import ca.claurendeau.repository.MessageRepository;
import ca.claurendeau.service.MessageService;

@Service
public class DemoService {

	Logger logger = LoggerFactory.getLogger(DemoService.class);
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private MessageRepository messageRepository;
	
	public void createInitialPersonnes() {
		messageService.saveMessage(new Message("Alex", "alex@donnemoi100svp"));
		messageService.saveMessage(new Message("Cousin", "Cousin@donnemoi100svp"));
		messageService.saveMessage(new Message("David", "David@donnemoi100svp"));
		messageService.saveMessage(new Message("Politicien", "Politicien@donnemoi100svp"));
	}
	
	public void test() {
		List<Message> messageFoundByNom = messageRepository.findByNom("David");
		logger.info(messageFoundByNom.get(0).toString());
		
		List<Message> messageFoundByEmail = messageRepository.findByEmail("Politicien@donnemoi100svp");
		logger.info(messageFoundByEmail.get(0).toString());
	}
}
